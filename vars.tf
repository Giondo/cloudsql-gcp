variable "REGION" {
	default = "northamerica-northeast1"
}
variable "ZONE" {
	default = "northamerica-northeast1-c"
}
variable "PROJECT" {
	default = "equifax-demo-dev"
}
variable "ZONES" {
	default = ["northamerica-northeast1-a", "northamerica-northeast1-b", "northamerica-northeast1-c"]
}
