terraform {
  backend "gcs" {
    bucket  = "equifax-terraform-states"
    prefix  = "terraform/state/cloudsql-gcp"
  }
}
